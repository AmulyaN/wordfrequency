# <a name="_bgvlrrmt0v8t"></a>Team.Money - Group Progress Report 3
## <a name="_3v696m9v2f0n"></a>Team Composition
- Himanshu Kumar
- Akul Kumar
- Carl Austin Dimalanta
- Brent Ilejay
- Ashishkumar kachhadiya
## User Story
Retrieve Translations of a semantic unit based on Document context, ChatGPT feature Tests

## Test Cases
### https://gitlab.com/mxm_yrmnk/wordfrequency/-/blob/Team_Money/test_pytest.py
- test_chatgpt_api_call
- test_single_word_translation
- test_multiple_translations
- test_no_translation_handling
- test_handle_different_languages
- test_error_handling_on_api_failure
- test_performance_metrics_recalculation
- test_chatgpt_api_call_with_no_translation_available
- test_chatgpt_api_call_with_single_translation
- test_chatgpt_api_call_with_multiple_translations
- test_display_translations_on_detail_screen
- test_display_semantic_and_translated_units
- test_performance_metrics_update_on_translation_attempts

## Logged Hours / Performance
### Meetings
April 27 - All Members - 4h &nbsp;  
May 1 - All Members - 1.4h
### Commits
Austin - 5 Commits &nbsp;   
Akul - 5 Commits &nbsp;  
Hima - 3 Commits &nbsp;  
Brent - 2 Commits &nbsp;  

### Tasks Done
##### [Create API Endpoint](https://gitlab.com/mxm_yrmnk/wordfrequency/-/work_items/22) 
Estimated Time: 1h &nbsp;  
Logged Time: .5h
##### [Create Configuration File Design](https://gitlab.com/mxm_yrmnk/wordfrequency/-/work_items/26)
Estimated Time: 1h &nbsp;  
Logged Time: .5h
##### [initialize pytest](https://gitlab.com/mxm_yrmnk/wordfrequency/-/work_items/32)
Estimated Time: 1h &nbsp;  
Logged Time: .5h
##### [Upload Use Case Diagram and Framework Diagram](https://gitlab.com/mxm_yrmnk/wordfrequency/-/work_items/33)
Estimated Time: 1h &nbsp;  
Logged Time: .5h
##### [Prompt ChatGBT to generate translation](https://gitlab.com/mxm_yrmnk/wordfrequency/-/work_items/43)
Estimated Time: 1h &nbsp;  
Logged Time: 1h
##### [Generate translation of document](https://gitlab.com/mxm_yrmnk/wordfrequency/-/work_items/44)
Estimated Time: 1h &nbsp;  
Logged Time: 1h
##### [Improve prompt engineering](https://gitlab.com/mxm_yrmnk/wordfrequency/-/work_items/45)
Estimated Time: 1h &nbsp;  
Logged Time: 1h
##### [Test sentence translation](https://gitlab.com/mxm_yrmnk/wordfrequency/-/work_items/46)
Estimated Time: 1h &nbsp;  
Logged Time: 1h
##### [Test translation of word with multiple languages](https://gitlab.com/mxm_yrmnk/wordfrequency/-/work_items/47)
Estimated Time: 1h &nbsp;  
Logged Time: 1h
##### [test_multiple_translations_handling()](https://gitlab.com/mxm_yrmnk/wordfrequency/-/work_items/49)
Estimated Time: 1h &nbsp;  
Logged Time: 1h
##### [test_chatgpt_api_call()](https://gitlab.com/mxm_yrmnk/wordfrequency/-/work_items/50)
Estimated Time: 1h &nbsp;  
Logged Time: 1h
##### [test_no_translation_handling()](https://gitlab.com/mxm_yrmnk/wordfrequency/-/work_items/51)
Estimated Time: 1h &nbsp;  
Logged Time: 1h
##### [test_original_and_translated_examples()](https://gitlab.com/mxm_yrmnk/wordfrequency/-/work_items/52)
Estimated Time: 1h &nbsp;  
Logged Time: 1h

### Tasks In Progress
##### [test_performance_metrics_recalculation()](https://gitlab.com/mxm_yrmnk/wordfrequency/-/work_items/53)
Estimated Time: 2h
##### [Add more Exhaustive Tests](https://gitlab.com/mxm_yrmnk/wordfrequency/-/work_items/88)
Estimated Time:1d

### Velocity
##### 
Sprint velocity = Number of tasks / Total hours

Sprint velocity = 13 tasks / 11 hours

Sprint velocity ≈ 1.18 tasks per hour

## Configuration design
To keep the API key secure and avoid exposing it in the code, we use the following configuration design:

1. Create a `.env` file in the project's root directory, and add the API key in the following format: API_KEY=your_openai_api_key_here

2. Add `.env` to the `.gitignore` file to ensure it is not tracked by Git and accidentally pushed to the repository.

3. Install the `python-dotenv` library if you haven't already:

```bash
pip install python-dotenv
```
4. In your Python code, use the following snippet to load the .env file and access the API key:

```
import os
from dotenv import load_dotenv

load_dotenv()
api_key = os.environ.get("API_KEY")
```

By following these steps, the API key will be securely stored in the .env file, which will not be included in the Git repository.

## Integration Tests Documentation
https://gitlab.com/mxm_yrmnk/wordfrequency/-/blob/Team_Money/Integration%20Tests%20Documentation.md

#680: Adv. Topics

## Prompt Engineering

Prompt engineering is a crucial aspect of designing a successful ESL (English as a Second Language) application using OpenAI's ChatGPT. It involves crafting effective prompts that elicit accurate, relevant, and helpful responses from the language model. Here are some key points to consider when designing prompts for the ESL application:

Clarity and specificity: Ensure that your prompts are clear and specific, so the language model understands the context and can generate meaningful responses. For example, when translating a word, provide the context in which the word is used to help the model generate accurate translations.

Role definition: Set up a system message at the beginning of the conversation to define the role of ChatGPT as an "ESL assistant." This helps set the context and makes it more likely for the model to provide relevant information.

###Example:

Development Language: python

{"role": "system", "content": "You are a helpful ESL assistant."}
Target language specification: Clearly specify the target language for translation to ensure accurate results. For example, instead of using "Translate the word 'apple' to another language," use "Translate the word 'apple' into French."
Requesting multiple translations and examples: When translating words with multiple meanings or uses, consider asking the model for all possible translations and their corresponding contexts and examples. This provides the user with a comprehensive understanding of the translated word.

###Example prompt:

Development Language: python

"As an English language learner, I need to translate the word '{word}' from the context '{document}' into {target_language}. Please provide all possible translations with their corresponding contexts and examples."
Experimentation and iteration: Continuously experiment with different prompt formulations and styles to achieve the best results. Monitor the quality of the generated translations and iterate on the prompts to improve their effectiveness over time.

Limiting response length: To obtain more concise answers, you can set a maximum token limit when calling the ChatGPT API. However, be cautious with very low limits, as they may truncate the response and result in incomplete or unclear information.

By employing effective prompt engineering techniques, you can enhance the performance of your ESL application, making it more useful and beneficial for users learning English as a second language.



