<a name="_vujkenjw6qwh"></a>Team.Money - Group Progress Report 1


1. ## <a name="_ts35hnyc4glt"></a>Team Composition
   - Himanshu Kumar
   - Akul Kumar
   - Carl Austin Dimalanta
   - Brent Ilejay
   - Ashishkumar kachhadiya

1. ## <a name="_6gq27lfq0ufd"></a>Team Manager - Week 1
   - Himanshu Kumar 

1. ## <a name="_v80j06wrt9nq"></a>Processed Agreed by Team:
   - Scrum Agile Methods
     - As a team, we have agreed to work together by having Zoom meetings twice a week to report our progress on the work done. Furthermore, when needed we agreed upon communicating over Discord for faster help with a specific task.
     - At the beginning of each meeting, we asked each team member about their progress on the task assigned to them. Then, we discussed any issues and/or clarifications needed. In the end, we went over the tasks to still perform.













1. ## <a name="_5h7dy08s7s0x"></a>Learning GitLab, Py-test, and SonarQube:

### <a name="_vzyarr4a5jcw"></a>Tasks to Perform: Learn each technology by studying the provided resources

`	`To make sure that the team has familiarized themselves with the required technologies for the project, I provided each member with online resources to study from. To verify my knowledge I asked the team members to pick a specific topic related to each technology and provide a paragraph describing such information.

- Learn GitLab
  - Introduction to GitLab Overflow <https://www.youtube.com/watch?v=enMumwvLAug&ab_channel=GitLab>
  - GitLab CI CD Tutorial for Beginners [Crash Course]

<https://www.youtube.com/watch?v=qP8kir2GUgo&ab_channel=TechWorldwithNana>

- Learn Py-test
  - Pytest Unit Testing Tutorial • How to test your Python code

<https://www.youtube.com/watch?v=YbpKMIUjvK8&ab_channel=pixegami>

- Learn SonarQube, the static code analysis tool that the professor uses
  - Static Code Analysis Using SonarQube for code quality

<https://www.sonarsource.com/open-source-editions/?msclkid=4658d4b2aa611aabf48ab08a266585e8>

- Static Code Analysis Using SonarQube for code quality

<https://www.youtube.com/watch?v=6VV8Mq5iUNg&ab_channel=GreenLearner>



### <a name="_qbpviq1b4ln"></a>Findings

#### <a name="_6hhlcvwnefp5"></a>GitLab
- **Merge Requests** by **Carl Austin Dimalanta**

Merge requests are a core feature of GitLab that simplifies the process of collaborating on code changes within a project. A merge request is created when a developer wants to merge their changes from one branch into another, typically the main branch. This process ensures that changes are reviewed, tested, and approved before they are integrated into the main codebase. When working on a project, developers can create feature branches, make changes, and push them to the GitLab repository. They can then create a merge request that details the changes made, the reasons behind them, and any additional context. Team members can review the changes, provide feedback, and suggest improvements. Once the changes are approved, the merge request can be merged into the target branch, ensuring a smooth integration of new features or bug fixes into the main codebase.

- **GitLab Issues** by **Brent Ilejay**

GitLab Issues can be created by team members or automatically for tracking and managing features, bugs, and other tasks. In order to create a new issue in GitLab, there is an issues tab where a “New Issue” button is. After clicking, you have the option to add a title and description, assign to member(s), set due dates, and labels for organization purposes. After creating an issue, members can organize and prioritize certain tasks and track progress. In addition, there is a comment section to allow for discussion with problems or feedback.

- **CI/CD Pipelines** by **Akul Kumar**

CI/CD is the combined practices of continuous integration and continuous delivery or continuous deployment. They are sometimes referred to collectively as continuous development or continuous software development. You can set up a pipeline on the GitLab project and use it to quickly deploy tests and demos. 

- **GitLab Container Registry** by **Ashishkumar kachhadiya**

This section of the video discusses GitLab's built-in Container Registry, which allows users to store and manage Docker images within their GitLab instance. The speaker explains how the Container Registry works, how to configure it, and how to push and pull images to and from the registry. The Container Registry can be used as a central location for sharing and deploying container images across teams and projects.
#### <a name="_ssgjp06hxd4v"></a>py-test
- **Test Parameterization** by **Carl Austin Dimalanta**

Test parameterization is a powerful feature in pytest that allows for more efficient testing by running the same test function multiple times with different input values. This can be particularly useful when testing functions that have numerous input combinations, helping to reduce redundancy in test code and making it easier to maintain. Parameterizing a test involves using the @pytest.mark.parametrize decorator to specify the input values and expected outputs for the test function. The test function's arguments will then be automatically populated with the provided values during test execution. This approach ensures that the test suite remains clean and easy to read while still providing comprehensive coverage for various input scenarios.

- **Text Fixtures** by **Brent Ilejay**

Py-test provided functions called, py-test fixtures, that provide data or services to tests. Fixtures can be used to set up test data, initialize resources, and organize after tests. In order to use a fixture for a test, you must define the function with @pytest.fixture decorator. After, the function could be called when including it in an argument. Fixtures are important for testing and it allows for more reliable and efficient tests.

- **Test Assertions** by **Akul Kumar**

Assertions are used to check that the result of a test matches the expected outcome, and are used to verify that the behavior of the code being tested is correct. You can do this by creating tests using the py-test library and creating tests with the expected outcome

- **Test Coverage** by **Ashishkumar kachhadiya**

This section of the video discusses test coverage in Pytest, which is a measure of how much of your code is covered by tests. The speaker explains how to use the coverage package with Pytest to generate a report of your test coverage, and demonstrates how to use the report to identify areas of your code that are not covered by tests. Test coverage can help you identify potential bugs and improve the quality of your code.
#### <a name="_ds1m4cmtrat7"></a>SonarQube
- **Code Coverage Analysis** by **Carl Austin Dimalanta**

Code coverage analysis is a critical aspect of ensuring the quality and reliability of a software project. SonarQube provides an easy-to-use interface to visualize and analyze code coverage data, helping developers identify areas of the code that may be lacking sufficient test coverage. When working on a project, SonarQube can be integrated into the CI/CD pipeline to automatically analyze the code coverage after each build. The results are displayed in the SonarQube dashboard, highlighting the percentage of code covered by tests, along with a detailed breakdown of which lines and branches are not covered. This information can be used to improve the test suite, ensuring that all essential parts of the codebase are tested and reducing the likelihood of introducing bugs or regressions.

- **Quality Gates** by **Brent Ilejay**

Quality gates allow the setting up of criteria for code that deems “acceptable” or “passing”. It prevents low-quality code from being merged into a large production branch. The overall and general workflow of utilizing Quality Gates include: creating a new Quality Gate, running code analysis, taking corrective action, re-running code analysis. To conduct the actual task, a user must: define the criteria, configure the quality gate, set up the project, analyze the code, review the results. Quality gates are important for ensuring code meets certain standards.

- **Dashboard & Reports** by **Akul Kumar**

You can use SonarQube to generate dashboards and reports that provide insights into the health and quality of your codebase, such as tests passed and compilation errors and teamwork stats.

- **Code Smells & Code Quality Metrics** by **Ashishkumar kachhadiya**

This section of the video covers code smells and code quality metrics in SonarQube, which are used to identify and measure the quality of your code. The speaker explains what code smells are, how they can be detected in SonarQube, and how they can be addressed to improve code quality. The speaker also discusses code quality metrics, such as complexity, maintainability, and test coverage, and explains how they can be measured and improved using SonarQube. Code smells and code quality metrics can help you identify areas of your code that need improvement and maintain the overall health of your codebase.
### <a name="_iajuzak2ivmx"></a>Metrics to Evaluate Team
- Zoom attendance
  - Carl Austin Dimalanta
    - Attended both Zoom meetings without being late
  - Akul Kumar
    - Attended both Zoom meetings without being late
  - Brent Ilejay
    - Attended both Zoom meetings without being late
  - Ashishkumar kachhadiya
    - Attended both Zoom meetings without being late
- Communication over Discord
  - Carl Austin Dimalanta
    - Communicated with team members in a timely manner when contacted
  - Akul Kumar
    - Communicated with team members in a timely manner when contacted
  - Brent Ilejay
    - Communicated with team members in a timely manner when contacted
  - Ashishkumar kachhadiya
    - Communicated with team members in a timely manner when contacted
- Task completed
  - Carl Austin Dimalanta
    - Completed task before deadline
  - Akul Kumar
    - Completed task before deadline
  - Brent Ilejay
    - Completed task before deadline
  - Ashishkumar kachhadiya
    - Completed task before deadline

1. ## <a name="_kxfbvir0o5y1"></a>Structures in GitLab Necessary to Deliver and Log Performance

- Pipelines: Continuous development
- Metrics Dashboard: Monitor code performance.
- Third-party tools integration if necessary
- Gitlab Pages: Host demos

1. ## <a name="_o9at41z6gva6"></a>Static Code Analysis Tool
   - SonarQube
     - Popular and widely-used tool for static code analysis
     - Comprehensive code analysis
       - Wide range of code analysis tools to identify issues such as security vulnerabilities or bugs.
     - Customizable quality profiles 
       - Allows custom quality profiles to specify specific rules and requirements for code analysis 
     - Real-time feedback
       - Catches issues early in the development process
     - Reporting and visualizations
       - Can create detailed reports and visualizations to understand the state of code and its progress








1. ## <a name="_cy8uq8sh8nmq"></a>Guidelines Document for Py-Test from the Recommendations


||Guidelines|
| :- | :- |
|Names|<p>- Test names should be descriptive</p><p>- Indicate the test used</p>|
|Fixtures|<p>- Function to provide reusable setup code for test</p><p>- Reduce code duplication for modularity</p>|
|Parameterized Tests|<p>- Run the same test with multiple sets of input data</p><p>- More comprehensive tests</p>|
|Assertions|<p>- Statements to verify expected behavior of code tested</p><p>- Check that the result of the code has the expected outcome</p>|
|Test Coverage|<p>- Measure amount of code is covered by tests</p><p>- Check missing cases</p>|