<a name="_bgvlrrmt0v8t"></a>Team.Money - Group Progress Report 2
## <a name="_3v696m9v2f0n"></a>Team Composition
- Himanshu Kumar
- Akul Kumar
- Carl Austin Dimalanta
- Brent Ilejay
- Ashishkumar kachhadiya
## <a name="_d02ciov4yjmw"></a>1. Guidelines Doc
`		`\*SEE BELOW\*
## <a name="_femp6djadc4z"></a>2. Project for the Unit Tests
- Story: US: - Retrieve Translations of a semantic unit based on Document context, ChatGPT feature Tests
## <a name="_dhwf3mjldrgh"></a>3. Unit Tests & Test script manual
## <a name="_sbxyuqhr7e6a"></a>Next step: Assign test cases to each member and develop unit tests 


|<p>**(1) Test case name:** Phrase translation</p><p></p><p>**Description:** Test the ability of the AI language model to retrieve translations of a phrase in a given document context.</p><p></p><p>**Test case steps:**</p><p>- Provide a document context containing a phrase in a non-English language. </p><p>- Verify that the AI language model can accurately translate the entire phrase and maintain the context of the original document.</p>|
| :- |
|<p>**(2) Test case name:** Multiple language document</p><p>**Description:** Test the ability of the AI language model to retrieve translations of multiple semantic units in a document containing multiple languages.</p><p></p><p>**Test case steps:**</p><p>- Provide a document context containing multiple semantic units in different languages.</p><p>- Verify that the AI language model can accurately identify the language of each semantic unit and provide accurate translations.</p>|
|<p>**(3) Test case name:** Homographs translation</p><p></p><p>**Description:** Test the ability of the AI language model to disambiguate homographs and retrieve the correct translation based on document context.</p><p></p><p>**Test case steps:**</p><p>- Provide a document context containing a homograph (a word with multiple meanings) in a non-English language.</p><p>- Verify that the AI language model can accurately identify the correct meaning of the homograph based on the context and provide an accurate translation.</p>|
|<p>**(4) Test case name:** Ambiguous translation</p><p></p><p>**Description:** Test the ability of the AI language model to identify ambiguous semantic units and provide multiple translation options based on document context.</p><p></p><p>**Test case steps:**</p><p>- Provide a document context containing an ambiguous semantic unit (a word or phrase with multiple possible translations) in a non-English language.</p><p>- Verify that the AI language model can accurately identify the possible translations based on the context and provide multiple options for the user to choose from.</p>|
|<p>**(5) Test case name:** Cross-lingual retrieval</p><p></p><p>**Description:** Test the ability of the AI language model to retrieve translations of a semantic unit across multiple languages.</p><p></p><p>**Test case steps:**</p><p>- Provide a document context containing a semantic unit in a non-English language.</p><p>- Verify that the AI language model can accurately translate the semantic unit into multiple languages, based on user preferences or settings.</p>|

## <a name="_7gflwzljq3xp"></a>4. Managerial Report


|**Manager of the week**|- Akul Kumar|
| :-: | :- |
|**Team Members**|<p>- Himanshu Kumar</p><p>- Akul Kumar</p><p>- Carl Austin Dimalanta</p><p>- Brent Ilejay</p><p>- Ashishkumar kachhadiya</p>|
|**Team Performance Metrics**|<p>- Test scripts manual</p><p>- Guidelines Documentation contribution</p><p>- Test Architecture contribution</p><p>- Define naming convention</p>|
|**Individual Member Metrics**|<p>- Test cases report</p><p>- Availability</p><p>- Communication</p>|
|**Individual Member Score**|<p>- Himanshu Kumar - 3hrs</p><p>- Carl Austin Dimalanta - 3hrs</p><p>- Brent Ilejay - 3hrs</p><p>- Ashishkumar kachhadiya - 1hr</p>|
|` `**Assignments**|<p>- Create a test case with specific flow step and acceptance criteria</p><p>- Contribution to the guidelines document</p><p>- Contribution to team meetings</p><p></p><p>*\*see below for issues broken down into tasks\**</p>|
|**Milestone Status**|<p>- Guidelines Document: Completed</p><p>- Test scripts manual: Completed</p><p>- Naming convention: Completed</p><p>- Test Architecture: Completed</p><p>- Unit Test - Code: In progress</p>|
|**Retrospective**|<p>- More communication with frequent meetings via Zoom or Discord.</p><p>- More coding next sprint instead of planning and creating documentation.</p>|




<a name="_by926d1sek7l"></a>Guideline Document

## <a name="_j4r04qz0q0u4"></a>Summary
The goal of this project is to develop a context-sensitive translation feature for ESL learners using the ChatGPT API. This feature will be integrated into the Detail Screen of the application, allowing users to translate specific words, phrases, or semantic units in a given document to better understand the context.

Key features of the project include generating translations based on the input document's context, displaying multiple translations with their corresponding contexts, handling cases when no translation is available, and presenting translations in a user-friendly format. Additionally, the system will display both original and translated contextual examples and recalculate performance metrics based on the usage of the translation option.

By providing accurate and contextually relevant translations, this project aims to enhance the learning experience for ESL learners, enabling them to better comprehend and engage with the content in their target language.

## <a name="_oc42b61qbdws"></a>Feature
- Retrieve translations of a semantic unit based on Document context
- ChatGPT-based context-sensitive translations for ESL learners.
- Display multiple translations and their corresponding contexts.
- Handling cases when no translation is available.
- Translation display format on the detail screen.
- Original and translated contextual examples.
- Recalculation of performance metrics based on translation usage.
<a name="_7e7u29sywe9y"></a>
Acceptance criteria
----------------------------
- The system will use ChatGPT API to translate words based on the context of the input document.
- If there are multiple translations available, the system will show all of them with their corresponding context in the input document.
- If there is no translation available for a word, the system will display a message or icon to inform the user.
- The translations will be displayed in a separate section on the interface, similar to Google, with details such as semantic unit, translated unit, languages, and examples.
- The original contextual example and corresponding translated examples will be shown.
- The system will recalculate performance metrics based on the use of the translation option, as it indicates that the contextual understanding still relies on the original language.

## <a name="_z13ipuxxvfj2"></a>Work Distribution

- Developer 1
1. Integrate the ChatGPT API into the system and set up the translation function.
1. Develop a module to retrieve all available translations for a given word.
1. Develop a module to extract the corresponding context of the translations from the input document.
- Developer 2
1. Implement a message or icon to inform the user when there is no translation available for a given word.
1. Develop a module to display the original contextual example and corresponding translated examples.
1. Work on improving the user interface of the translation section, including the display of semantic units, translated units, languages, and examples.
- Developer 3
1. Develop a performance metric module to track the translation attempts for each word.
1. Integrate the performance metric module with the translation function.
1. Work on refining the performance metric calculation algorithm based on the use of the translation option.
- Quality Assurance
1. Write test cases for all the functionalities of the translation module.
1. Conduct manual testing to ensure the accuracy and completeness of the translations and their corresponding contexts.
1. Conduct performance testing to validate the performance metrics and identify any bottlenecks or issues.

## <a name="_dm8rhu9nun3t"></a>Test Cases

Test Case 1: Single translation

Input: A document containing a word with a single translation in context.

Expected Output: The system should generate and display the correct translation along with the corresponding contextual example.

Input: retrieveTranslations("Bonjour", "en")

Expected output: "Hello"


Test Case 2: Multiple translations

Input: A document containing a word with multiple translations in context.

Expected Output: The system should generate and display all possible translations along with their corresponding contextual examples.

Input: retrieveTranslations("Je m'appelle Jean", "en")

Expected output: "My name is Jean"

Test Case 3: No translation available

Input: A document containing a word with no available translation.

Expected Output: The system should follow the agreed-upon method of delivering this information to the user (e.g., displaying a message on the screen or an icon).

Input: retrieveTranslations("Ich spreche Deutsch und Englisch", "fr")

Expected output: "Je parle allemand et anglais"

Test Case 4: Translation display format

Input: Any document containing a word/phrase/semantic unit requiring translation.

Expected Output: The system should display the translations in the correct format, including the semantic unit, translated unit, languages, and examples.

Input: retrieveTranslations("El libro está en la mesa", "en")

Expected output: "The book is on the table"

Test Case 5: Display of original and translated contextual examples

Input: Any document containing a word/phrase/semantic unit requiring translation.

Expected Output: The system should display both the original contextual example and the corresponding translated examples.

Input: retrieveTranslations("La manzana es roja", "en")

Expected output: ["The apple is red", "The handle is red"]

Full functionality test:  Integration with ChatGPT API

Input: A document containing a word/phrase/semantic unit requiring translation.

Expected Output: The system should successfully use the ChatGPT API to generate the appropriate translations based on the document context.


**Py-test example:**

*# Import the function*

*from my\_module import retrieve\_translations*

*# Define the test*

*def test\_single\_word\_translation():*

`    `*# Define the input and expected output*

`    `*document\_context = 'Bonjour'*

`    `*target\_language = 'en'*

`    `*expected\_output = 'Hello'*

`    `*# Call the function and verify the output*

`    `*result = retrieve\_translations(document\_context, target\_language)*

`    `*assert result == expected\_output*


## <a name="_lr5t2zhrk1to"></a>Naming Convention
- **Variables**: Camel Case *(camelCase)*
- **Branches:** Snake Case *(snake-case)*
- **Test:** Begin name with ‘test’ *(testfunction)*
- **Files:** Use ‘.’ *(file.name)*

## <a name="_3m85x2cmvz6s"></a>Test Architecture
After identifying the requirements, we create test architecture guidelines to follow:

1. **Create a test plan:** Create a test plan
1. **Define the testing tools:** Based on the testing plan, identify the testing tools needed to execute the tests.
1. **Create the test cases:** Create test cases for each of the identified scenarios.
1. **Create the test data**: The test data should be created or sourced to match the scenarios outlined in the test cases, including the expected outcome.
1. **Execute the tests**: Execute the test cases manually or using automated testing tools.
1. **Analyze the test results:** Analyze the test results to identify any issues or defects.
1. **Provide a test report**: Create a test report summarizing the test results.
1. **Continuous testing:** Continuously test the feature as new updates or changes are made.
